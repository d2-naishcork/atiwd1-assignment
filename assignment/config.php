<?php

// Parts of the code below in the 'outPut' function, is used from Prakash's uploaded example and is not entirely my own

// timestamp array in seconds (2 hours)
define('UPDATE_LIMITER', '7200');

// The 26 individual currency code arrays
define('LIVE', array(
    'AUD', 'BRL', 'CAD', 'CHF',
    'CNY', 'DKK', 'EUR', 'GBP',
    'HKD', 'HUF', 'INR', 'JPY',
    'MXN', 'MYR', 'NOK', 'NZD',
    'PHP', 'RUB', 'SEK', 'SGD',
    'THB', 'TRY', 'USD', 'ZAR' 

));

// set a constant array holding format vals
define('FRMTS', array(
	'xml', 'json'
));

// params array
define('PARAMS', array(
	'to', 'from', 'amnt', 'format'
));

// error_hash to hold error numbers and messages
// Parts of the code below in the 'outPut' function, is used from Prakash's uploaded example and is not entirely my own
define ('ERROR_HASH', array(
	1000 => 'Required parameter is missing',
	1100 => 'Parameter not recognized',
	1200 => 'Currency type not recognized',
	1300 => 'Currency amount must be a decimal number',
	1400 => 'Format must be xml or json',
	1500 => 'Error in Service',
	2000 => 'Action not recognized or is missing',
	2100 => 'Currency code in wrong format or is missing',
	2200 => 'Currency code not found for update',
	2300 => 'No rate listed for currency',
	2400 => 'Cannot update base currency',
	2500 => 'Error in service'
));

?>