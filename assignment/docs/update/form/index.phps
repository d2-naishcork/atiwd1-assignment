<?php
require_once("../../functions.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <title>Advanced Technologies in Web Development Assignment</title>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <script src='main.js'></script>
    </head>
    <style>
    h1 {
        color: cornflowerblue;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        text-decoration-line: underline;
        align-content: center;
        width: 450px;
    }

    h2 {
        color: cadetblue;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        text-decoration-line: underline;
    }

    p {
        color: cornflowerblue;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        text-decoration-line: underline;
    }

    select {
        margin-bottom: 50px;
        margin-top: 50px;
        margin-right: 10px;
    }
    </style>
    <body>
        <form id="form1" action="#">
            <h1>Advanced Technologies in Web Development Currency Conversion API</h1>
            <h2>DELETE, POST & PUT:</h2>
                <div id="actions">
                    <p>Delete:</p><input id="del" type="radio" value="del" name="action">
                    <p>Post:</p><input id="post" type="radio" value="post" name="action"> 
                    <p>Put:</p><input id="put" type="radio" value="put" name="action"> 
                </div>
                    <select id='CurCode'>
                        <?php
                            populateDropdown()
                        ?>
                        <input type="submit" name="submit" value="Submit">
                    </select>
                <div> 
                    <textarea id="returntext" rows="10" cols="60" ></textarea>
                </div> 
        </form>
    </body>
</html>
