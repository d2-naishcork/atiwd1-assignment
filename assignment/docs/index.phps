<?php
// I was having issues with (which seems to be fixed now) isa.cems.uwe.ac.uk is causing random errors to occur in some of tests that you shared with us.
// If you get a response which doesn't match what you were testing, PLEASE change it from
// 'isa.cems.uwe.ac.uk' to 'fetstudy.uwe.ac.uk', and you will then see the correct information.
// If 'fetstudy.uwe.ac.uk' then produces an unknown error also, please use 'isa.cems.uwe.ac.uk'



// set API Endpoint and API key
require_once('config.php'); 
require_once('functions.php');

// 'checking();' checks if the file exists, used in 'functions.php' 
checking();

// 'rates();' uses API key from fixer.io, timestamp and access key in 'functions.php' 
rates();

// 'currencies();' loads currencies and runs the 'currencies();' function, in the 'functions.php' 
currencies();

// 'url_error_check();' runs the 'url_error_check' function and checks for specific errors by producing an error code
// if an abnormality is found
url_error_check();

// 'echo outPut();' outputs the user's results using the 'outPut' function in 'functions.php
echo outPut();

?>