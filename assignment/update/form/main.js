window.onload=function(){
    document.getElementById('form1').addEventListener('submit', function(e){
        e.preventDefault();
        actionRequest();
    });
}

// The actionRequest function, requests different values when running the URL in a web browser. When selecting either the 'Put, Post or Delete'
// radio buttons on the webpage, this will respond with the option the user has chose by linking to the relevant functions in the 'index.php' file
// and 'functions.php' file, and place it into the URL by adding it next to the 'action' section of the URL, similar also with the 'curCode' (&cur)
function actionRequest()
{
var select = document.getElementById("CurCode");
var curCode = select.options[select.selectedIndex].value;
var radioIds = ["del", "put", "post"];
var action = getRadioValue(radioIds);


var url = window.location.origin + '/~d2-naishcork/atwd1/assignment/update/index.php?action=' + action + '&cur=' + curCode;
var xhttp = new XMLHttpRequest();

xhttp.onreadystatechange = function()
{
if (this.readyState == 4 && this.status == 200){
    document.getElementById('returntext').innerHTML = this.responseText;
}
}

xhttp.open("GET", url, true);
xhttp.send();
}

// The 'getRadioValue' function gets the value of the selected radio button on the form 'POST, PUT or DELETE' and when the 'Submit' button
// is clicked, will run the chosen option and display the information in the text area
function getRadioValue(radioIds){
    var action;
    radioIds.forEach(radioId => {
        if (document.getElementById(radioId).checked){
            action = document.getElementById(radioId).value;
        }
    });
    return action;

}