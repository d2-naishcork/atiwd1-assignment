<?php
//Call validation here
if (!validate()){
    exit();
}

$action = $_GET['action'];
$curCode = $_GET['cur'];

// If the action chosen is either 'del', 'put' or 'post', run relevant function when using form or URL
if ($action=='del')
{
    deleteCurrency($curCode);
}

if ($action=='put')
{
    putCurrency($curCode);

}

if ($action=='post')
{
    postCurrency($curCode);

}

// The 'deleteCurrency' function searches for the 'current_rates.xml', loads it when found, finds the selected currency code
// the user has selected, deletes the code from the 'current_rates.xml' file (replaces the 1 with a 0) and saves the
// 'current_rates.xml' file
function deleteCurrency($CurCode)
{
    $dom = new DOMDocument('1.0');
    $dom->load('../current_rates.xml');
    
    $xpath = new DOMXPath($dom);
    $codetoDel = $xpath->query("//CCode[CurCode='{$CurCode}']/Live")[0];
    $codetoDel->nodeValue = "0";
    $dom->save('../current_rates.xml');
    return response("del", $CurCode);

}

// The 'putCurrency' function searches fixer.io for the currency code and value using the API (access_key) key, searches for the 'current_rates.xml', 
// loads it when found, finds the selected currency code and value the user has selected, places the code, value and live node in place and converts the rate, 
// saving an updated version of the 'current_rates.xml' file
function putCurrency($CurCode)
{
    $access_key = '4e966d6188be23227a4b55f41038164d';
    $json = file_get_contents('http://data.fixer.io/api/latest?access_key='.$access_key.'&symbols=GBP,'.$CurCode);
    $jsonDecode = json_decode($json);

    $dom = new DOMDocument('1.0');
    $dom->load('../current_rates.xml');
    
    $xpath = new DOMXPath($dom);
    $codeElement = $xpath->query("//CCode[CurCode='{$CurCode}']")[0];

    $curValue = $codeElement->getElementsByTagName("CurValue")->item(0);
    $liveNode = $codeElement->getElementsByTagName("Live")->item(0);

    $convertRate = 1 / $jsonDecode->rates->GBP;
    $convertedRate = $jsonDecode->rates->{$CurCode} * $convertRate;
    $curValue->nodeValue = $convertedRate;
    $liveNode->nodeValue = "1";
    $dom->save('../current_rates.xml');
    return response("put", $CurCode, $jsonDecode);


}

// The 'postCurrency' first checks to see if the 'Live' node value is 1 or 0, if it's 0 (deleted), it will return an error code
// ('Not live' is shown in place of the error code as I ran out of time to implement the 2000s error codes).
// If it is live, then it will post the latest rate and value of that currency to the 'current_rates.xml' file using
// again, the fixer.io site and API key for the latest up-to-date currencies
function postCurrency($CurCode)
{
    $dom = new DOMDocument('1.0');
    $dom->load('../current_rates.xml');
    
    $xpath = new DOMXPath($dom);
    $codeElement = $xpath->query("//CCode[CurCode='{$CurCode}']")[0];

    $curValue = $codeElement->getElementsByTagName("CurValue")->item(0);
    $liveNode = $codeElement->getElementsByTagName("Live")->item(0);

    if ($liveNode->nodeValue == "0"){
        print_r("Not live");
        //Return error code
    } else {
        $access_key = '4e966d6188be23227a4b55f41038164d';
        $json = file_get_contents('http://data.fixer.io/api/latest?access_key='.$access_key.'&symbols=GBP,'.$CurCode);
        $jsonDecode = json_decode($json);

        $convertRate = 1 / $jsonDecode->rates->GBP;
        $convertedRate = $jsonDecode->rates->{$CurCode} * $convertRate;
        $curValue->nodeValue = $convertedRate;
        $dom->save('../current_rates.xml');
        return response("post", $CurCode, $jsonDecode);
    }


}

// The 'response' function detects which radio button the user has clicked and what action they have chosen
// (Post, Put or Delete). It will then display in the text area which action type they have chosen, 
// what the date and time is when they submitted it and the rate, country code, currency, location and value
// of it is
    function response($action, $curCode, $response = "")
    {
        if ($response != ""){
            $convertRate = 1 / $response->rates->GBP;
            $convertedRate = $response->rates->{$curCode} * $convertRate;
        }
        $currentRates = simplexml_load_file('../current_rates.xml');
        $countries = simplexml_load_file('../currencies.xml');

        $codes = $currentRates->xpath("//CCode[CurCode='{$curCode}']");
        $country = $countries->xpath("//currency[ccode='{$curCode}']");
        $xmlWriter = new SimpleXMLElement("<action></action>");
        $xmlWriter->addAttribute('type', $action);
        $xmlWriter->addChild('at', gmdate("d F Y H:i", time()));
            
        if ($action == "post" || $action == "put"){
            $xmlWriter->addChild('rate', $convertedRate);
        }

        if ($action == "post"){
            $tsArray = array();

            foreach (glob('../current_rates_*') as $lastFile){
                $ts = $lastFile;
                $ts = strstr($lastFile, "current_rates_");
                $ts = strstr($lastFile, ".xml", true);
                $ts = substr($ts, strpos($ts, "_rates_") + 7);
                array_push($tsArray, (int) $ts);
            }
            if (!empty($tsArray))
            {
                $lastTs = max($tsArray);
            }
            if (isset($lastTs) && file_exists("../current_rates_{$lastTs}.xml"))
            {
                $previousRates = simplexml_load_file("../current_rates_{$lastTs}.xml");
                $prevRate = $previousRates->xpath("//CCode[CurCode='{$curCode}']")[0];
                $xmlWriter->addChild('old_rate', $prevRate->CurValue);
            } else {
                $xmlWriter->addChild('old_rate');

            }
            

        }

        if ($action == "del"){ 
            $xmlWriter->addChild('code', $curCode);
        } else {
            $curr = $xmlWriter->addChild('curr');
            $curr->addChild('code', (string) $codes[0]->CurCode);
            $curr->addChild('name', (string) $country[0]->cname);
            $curr->addChild('loc', (string) $country[0]->cntry);
        }
        header('Content-type: text/xml');
        echo $xmlWriter->asXML();
    }

function validate(){
    return true;
}
?>