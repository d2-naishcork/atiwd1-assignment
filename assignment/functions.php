<?php
    @date_default_timezone_set("GMT");

    function rates()
{
    $access_key = '4e966d6188be23227a4b55f41038164d';
    
    $timestamp = time();

    $json = file_get_contents('http://data.fixer.io/api/latest?access_key='.$access_key );

    $tsArray = array();

    // Checks to see if there's a current_rates.xml file existing with an old timestamp on,
    // if yes, then loads that existing file    
    foreach (glob('current_rates_*') as $lastFile){
        $ts = $lastFile;
        $ts = strstr($lastFile, "current_rates_");
        $ts = strstr($lastFile, ".xml", true);
        $ts = substr($ts, strpos($ts, "_rates_") + 7);
        array_push($tsArray, (int) $ts);
    }
    if (!empty($tsArray))
    {
        $lastTs = max($tsArray);
    }
    
    if (isset($lastTs) && file_exists("current_rates_{$lastTs}.xml"))
    {
        $currentRates = simplexml_load_file("current_rates_{$lastTs}.xml");
    }
    // Decode the 'json' response
    $exchangeRates = json_decode($json, true);
    
    $ER = $exchangeRates['rates'];
    $convRate = 1 / $ER['GBP'];

    $xmlWriter = new XMLWriter();
    $xmlWriter->openURI('current_rates.xml');
    $xmlWriter->startDocument('1.0', 'UTF-8');
    $xmlWriter->startElement('rates');
    $xmlWriter->writeAttribute('base', 'GBP');
    $xmlWriter->writeAttribute('ts', $timestamp);
    
    
    foreach($ER as $CurrencyCode=>$CurrencyValue)
    {
        if (isset ($currentRates))
        {
            $isLive = $currentRates->xpath("//*[CurCode='{$CurrencyCode}']/Live")[0];
        }
        else
        {
            $isLive = 1;
        }
        $xmlWriter->startElement('CCode');
        $xmlWriter->startElement('CurCode');
        $xmlWriter->text($CurrencyCode);
        $xmlWriter->endElement();
        $xmlWriter->startElement('CurValue');
        $xmlWriter->text($CurrencyValue * $convRate);
        $xmlWriter->endElement();
        $xmlWriter->startElement('Live');
        $xmlWriter->text((string)$isLive);
        

        $xmlWriter->endElement();
        $xmlWriter->endElement();

    }

    $xmlWriter->endElement();
    $xmlWriter->endDocument();
}

// Small parts of the code below in the 'currencies' function, is used from Prakash's uploaded example and is not entirely my own

// The function 'currencies' loads the currencies from 'currency-iso.org' and loads it into the currencies.xml file, getting all the
// currency codes
function currencies()
{
$xml=simplexml_load_file('http://www.currency-iso.org/dam/downloads/lists/list_one.xml') or die("Error: Cannot create object");
$xmlWriter = new XMLWriter();
$xmlWriter->openURI('currencies.xml');
$xmlWriter->startDocument("1.0");
$xmlWriter->startElement("currencies");
// get all the currency codes
$codes = $xml->xpath("//CcyNtry/Ccy");
$ccodes = [];
// make array with unique currency codes
foreach ($codes as $code) {
	if (!in_array($code, $ccodes)) {
		$ccodes[] = (string) $code;
	}
}

foreach ($ccodes as $ccode) 
{ 
	$nodes = $xml->xpath("//Ccy[.='$ccode']/parent::*");
	
	$cname =  $nodes[0]->CcyNm;
	
	// start by writing out the nodes and values
	$xmlWriter->startElement("currency");
    $xmlWriter->startElement("ccode");
    $xmlWriter->text($ccode);
		$xmlWriter->endElement();
		$xmlWriter->startElement("cname");
		$xmlWriter->text($cname);
		$xmlWriter->endElement();
		$xmlWriter->startElement("cntry");
		
			$last = count($nodes) - 1;
			
			// by using the same code, you can group the countries together
			foreach ($nodes as $index=>$node) {
				$xmlWriter->text(ucwords((strtolower($node->CtryNm))));
				if ($index!=$last) {$xmlWriter->text(', ');}
			}
            $xmlWriter->endElement();
        
        $xmlWriter->endElement();
}
$xmlWriter->endDocument();
$xmlWriter->flush();
}


// The code below in the 'generate_error' function, is used from Prakash's uploaded example and is not entirely my own

// function to return xml or json errors
function generate_error($eno, $format='xml') 
{
	$msg = ERROR_HASH["$eno"];
	
	if ($format=='json') {
		$json = array('conv' => array("code" => "$eno", "msg" => "$msg"));
		
		$out = header('Content-Type: application/json');
		$out .= json_encode($json, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
	}
	else {
		$xml =  '<?xml version="1.0" encoding="UTF-8"?>';
		$xml .= '<conv><error>';
		$xml .= '<code>' . $eno . '</code>';
		$xml .= '<msg>' . $msg . '</msg>';
		$xml .= '</error></conv>';
	
		$out = header('Content-type: text/xml');
		$out .= $xml;
	}
	return $out;
}

// The 'checking' function checks if the file exists, if it does, it updates the current_rates file name with the new timestamp
function checking()
{
    clearstatcache();
    if(file_exists('current_rates.xml'))
    {
        $xmlCR = simplexml_load_file('current_rates.xml');

        $ts = $xmlCR[@ts];

        $update = $ts + UPDATE_LIMITER;

        // This unfortunately causes the 'DEL' function to not work properly based on the fact that this is renaming the current_rates.xml,
        // which then sets the 'Live' for the currency from '0' when deleted, back to '1' as the file previously no longer exists after 2 hours
        // has passed or the currency gets deleted
        if(time() >= $update)
        {
            rename('current_rates.xml','current_rates_'.$ts.'.xml');
            rates();
        }

        else
        {
            currencies();
        }
    } else {
        echo 'checking';
        echo generate_error(1500, $_GET['format']);   
        exit();
    }
}

// Parts of the code below in the 'url_error_check' function, is used from Prakash's uploaded example and is not entirely my own

// The 'url_error_check' function loads the 'current_rates.xml' file and checks against the error codes below to see if any part of the
// URL are missing important information, such as currency codes
function url_error_check()
{
    if (file_exists('current_rates.xml'))
        {
        $currentRates = simplexml_load_file('current_rates.xml');
        } 
        else 
        {
            echo 'url';
        echo generate_error(1500, $_GET['format']);   
        exit();
        }
        
    if (!isset($_GET['format']) || empty($_GET['format'])) 
        {
            $_GET['format'] = 'xml';
        }
    // make sure the PARAM values match the keys in $GET
    if (count(array_intersect(PARAMS, array_keys($_GET))) < 4) 
        {
            echo generate_error(1000, $_GET['format']);
            exit();
        }
    // make sure there are no extra params
    if (count($_GET) > 4) 
        {
            echo generate_error(1100, $_GET['format']);
            exit();
        }

    // make sure that $amnt is not a two digit decimal value (this can be an integer)
    if (!preg_match('/^\d+(\.\d{1,2})$/', $_GET['amnt'])) 
        {
            echo generate_error(1300, $_GET['format']);
            exit();
        }

    // check for format values that are allowed
    if (!in_array( $_GET['format'], FRMTS)) 
        {
            echo generate_error(1400);
            exit();
        }

    if (isset ($currentRates))
        {
            $toCode = $_GET['to'];
            $fromCode = $_GET['from'];
            $toLive = $currentRates->xpath("//*[CurCode='{$toCode}']/Live")[0];
            $fromLive = $currentRates->xpath("//*[CurCode='{$fromCode}']/Live")[0];
        }
        else
        {
            $toLive = 1;
            $fromLive = 1;
        }

    // throw this error if $to and $from are not recognized currencies
    if ($toLive == '0' || $fromLive == '0') 
        {
        echo generate_error(1200, $_GET['format']);
        exit();
        }
}

// Parts of the code below in the 'outPut' function, is used from Prakash's uploaded example and is not entirely my own

// The outPut function below, outputs the xml or json to the webpage, showing the user full details about what they have calculated,
// using the URL or form
function outPut()
{    
    // load the current_rates xml file as a simple xml object
    $xmlRate = simplexml_load_file('current_rates.xml');

    // load the currencies xml file as a simple xml object
    $xmlCurrencies = simplexml_load_file('currencies.xml');
    
    // check for allowed format values
    if (!isset($xmlRate) || !isset($xmlCurrencies))
    {
        echo 'output';
        echo generate_error(1500, $_GET['format']);   
        exit();
    }

    $fromRate = $xmlRate->xpath('//rates/CCode[CurCode="' . $_GET['from'] . '"]');
    $toRate =  $xmlRate->xpath('//rates/CCode[CurCode="' . $_GET['to'] . '"]');

    // set the rate to 1.00 if 'to' and 'from' are the same
    if ($_GET['from']==$_GET['to'])
    {
	    $rate = 1.00;
	    $conv =  $_GET['amnt'];
    }    
    else
    {
        // calculate the related conversion rate
        $rate = (float) $toRate[0]->CurValue / (float) $fromRate[0]->CurValue;
        
        // calculate the actual conversion of the 2 currencies/countries
        $conv = $rate * $_GET['amnt'];
    }    
    
    $from = $xmlCurrencies->xpath('//currencies/currency[ccode="' . $_GET['from'] . '"]');
    $to = $xmlCurrencies->xpath('//currencies/currency[ccode="' . $_GET['to'] . '"]');
    
    $fromamnt = $_GET['amnt'];
    
    // If the format in the URL ends in 'json', then show webpage in 'json' format
    if ($_GET['format'] == 'json')
    {
        $json = array('conv' => array(
            // Convert the timestamp array to Day-Month-Year and Hours-Minutes-Seconds in 'json' format
            "at" => date("d-m-y H:i:s"), 
            // Grab and display current conversion rate
            "rate" => $rate,
            
            // Display 'from' tab on webpage in 'json'
            "from" => array(
                // Retrieve Currency Code (ccode) from currencies.xml file (from)
                "curcode" =>(string) $from[0]->ccode,
                // Retrieve Country Name (cname) from currencies.xml file (from)
                "curr" => (string)$from[0]->cname,
                // Retrieve Country (cntry) from currencies.xml file (from)
                "loc" => (string)$from[0]->cntry,
                // Apply decimal format to 'amnt' (from)
                "amnt" => number_format($fromamnt, 2,'.', '')
            ),

            // Display 'to' tab on webpage in 'json'
            "to" => array(
                // Retrieve Currency Code (ccode) from currencies.xml file and convert currency (to)
                "curcode" => (string)$to[0]->ccode,
                // Retrieve Country Name (cname) from currencies.xml file and convert currency (to)
                "curr" => (string)$to[0]->cname,
                // Retrieve Country (cntry) from currencies.xml file and convert currency (to)
                "loc" => (string)$to[0]->cntry,
                // Apply decimal format to 'amnt' and convert currency (to)
                "amnt" => number_format($conv, 2,'.', '')
            )
            
            ));
        
        $out = header('Content-Type: application/json');
        $out .= json_encode($json, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }
    // If the format in the URL ends in 'xml', then show webpage in 'xml' format
    else 
    {
        $xml = new SimpleXMLElement("<conv></conv>");
        // Convert the timestamp array to Day-Month-Year and Hours-Minutes-Seconds in 'xml' format
        $xml->addChild('at', date("d-m-y H:i:s"));
        // Grab and display current conversion rate
        $xml->addChild('rate', $rate);
        
        // Display 'from' tab on webpage in 'xml'
        $fromXML = $xml->addChild('from');
        // Retrieve Currency Code (ccode) from currencies.xml file (from)
        $fromXML->addChild('curcode', $from[0]->ccode);
        // Retrieve Country Name (cname) from currencies.xml file (from)
        $fromXML->addChild('curr', $from[0]->cname);
        // Retrieve Country (cntry) from currencies.xml file (from)
        $fromXML->addChild('loc', $from[0]->cntry);
        // Apply decimal format to 'amnt' (from)
        $fromXML->addChild('amnt', number_format($fromamnt, 2,'.', ''));
        
        // Display 'to' tab on webpage in 'xml'
        $toXML = $xml->addChild('to');
        // Retrieve Currency Code (ccode) from currencies.xml file and convert currency (to)
        $toXML->addChild('curcode', $to[0]->ccode);
        // Retrieve Country Name (cname) from currencies.xml file and convert currency (to)
        $toXML->addChild('curr', $to[0]->cname);
        // Retrieve Country (cntry) from currencies.xml file and convert currency (to)
        $toXML->addChild('loc', $to[0]->cntry);
        // Apply decimal format to 'amnt' and convert currency (to)
        $toXML->addChild('amnt', number_format($conv, 2,'.', ''));

        $out = header('Content-type: text/xml');

        return $xml->asXML();   
    }
    return $out;

}
// This function populates the dropdown box in the form displaying all the currency codes (CurCode).
// This allows the user to select the currency code (CurCode) of choice to either DELETE, POST or PUT, or
// all 3 if they do it in order (DELETE, POST then PUT).
function populateDropdown()
{
    echo $_SERVER['REQUEST_URI'];
    $currentRates = simplexml_load_file('../../current_rates.xml');
    // echo $currentRates->asXML();
    $codes = $currentRates->xpath('//CurCode');
    // print_r($codes);
    
    foreach($codes as $code)
    {
        echo "<option value='".$code."'>".$code."</option>";
    }

}